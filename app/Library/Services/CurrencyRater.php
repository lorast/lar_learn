<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/9/17
 * Time: 11:38 AM
 */

namespace App\Library\Services;


use Mockery\Exception;

class CurrencyRater
{
    const API_URL = "api.fixer.io/latest";

    /**
     * @param array $curr_arr
     * @param string $base_curr
     * @return array
     */
    public function oneToManyCompare(array $curr_arr, string $base_curr) : array
    {
        $base = '';
//        $curr_arr = [];

        $base = 'base=' . strtoupper($base_curr);
        $symbols = 'symbols=' . strtoupper(implode(',', $curr_arr));

        $url_part = $base . '&' . $symbols;

        try {
            $json = $this->getJsonFromApi($url_part);
        } catch (Exception $exception) {
            die($exception->getMessage());
        }
        return $json['rates'];
    }

    /**
     * @param array $curr_arr
     * @return array
     */
    public function ManyToManyCompare(array $curr_arr) : array
    {
        $first_value = $second_value = '';
        $rates_arr = [];
        $rev_curr_arr = array_reverse($curr_arr);

        foreach ($curr_arr as $value) {
            $first_value = 'base=' . array_shift($curr_arr);
            $second_value = 'symbols=' . array_shift($rev_curr_arr);

            $url_part = $first_value . htmlspecialchars_decode('&') . $second_value;

            try {
                $json = $this->getJsonFromApi($url_part);
            } catch (Exception $exception) {
                die($exception->getMessage());
            }

            array_push($rates_arr, $json);

        }

        return $rates_arr;
    }

    /**
     * Get JSON data from remote server
     *
     * @param string $url
     * @return mixed
     */
    private function getJsonFromApi(string $url) : array
    {
        $curl = '';
        $json_data = [];

        $curl = self::API_URL . '?' . $url;
//        var_dump($curl_res);die();
        if( $curl_res = curl_init($curl) ) {
            curl_setopt($curl_res, CURLOPT_URL, $curl);
            curl_setopt($curl_res, CURLOPT_RETURNTRANSFER,true);
            if (curl_exec($curl_res) === false) {
                echo curl_error($curl_res);
                die;
            }
            $out = curl_exec($curl_res);
            $content = $out;
            curl_close($curl_res);
        }

        $json_data = json_decode($content, true);

        return $json_data;
    }
}