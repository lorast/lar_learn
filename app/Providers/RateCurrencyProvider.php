<?php

namespace App\Providers;

use App\Library\Services\CurrencyRater;
use Illuminate\Support\ServiceProvider;

class RateCurrencyProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\CurrencyRater', function ($app) {
            return new CurrencyRater();
        });
    }
}
